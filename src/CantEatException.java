public class CantEatException extends RuntimeException {
    public CantEatException() {
        super("Nevar šo ēst!");
    }
}
