import java.time.LocalDateTime;

public class Pasta extends Meal {
    public Pasta() {
        super(10, 16, MealType.LUNCH);
    }

    @Override
    public String getName() {
        return "Pasta";
    }

    @Override
    public String getInstrument() {
        return "Fork";
    }
}

