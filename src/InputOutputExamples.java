import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class InputOutputExamples {
    public static void main(String[] args) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("io.txt"));) {
            writer.write("Hello");
        }
        io("io.txt");
        try (BufferedReader reader = new BufferedReader(new FileReader("io.txt"))) {
            System.out.println(reader.readLine());
        }
        nio("nio.txt");
    }

    private static void io(String filename) throws IOException {
        BufferedWriter writeAgain = new BufferedWriter(new FileWriter(filename, true));
        writeAgain.append(' ');
        writeAgain.append("World");
        writeAgain.close();
    }

    private static void nio(String filename) throws IOException {
        Path path = Paths.get(filename);
        byte[] strToBytes = "Hello".getBytes();
        Files.write(path, strToBytes);
        String firstLine = Files.readAllLines(path).get(0);
        System.out.println(firstLine);
    }
}
