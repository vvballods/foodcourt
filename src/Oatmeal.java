import java.time.LocalDateTime;

public class Oatmeal extends Meal {

    public Oatmeal() {
        super(6, 10, MealType.BREAKFAST);
    }

    @Override
    public String getName() {
        return "Oatmeal";
    }

    @Override
    public String getInstrument() {
        return "Spoon";
    }
}

