import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<String> elements = new ArrayList<>();
        elements.add("test");
        for (int i = 0; i < elements.size(); i++ ) {
            elements.get(i);
        }
        for (String element: elements) {
            System.out.println("test");
        }
        Scanner s = new Scanner(System.in);
        while (true) {
            printMenu();
            String userInput = s.next();
            if ("q".equals(userInput)) {
                break;
            }

            try {
                switch (userInput) {
                    case "1":
                        new Oatmeal().eat();
                        break;
                    case "2":
                        new Pasta().eat();
                        break;
                    case "3":
                        new Salad().eat();
                        break;
                    default:
                        System.out.println("Nepareiza ievade! Meģini vēlreiz.");
                }
            } catch (CantEatException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static void printMenu() {
        System.out.println("Menu:");
        System.out.println("1 - Oatmeal");
        System.out.println("2 - Pasta");
        System.out.println("3 - Salad");
        System.out.println("Ko Tu vēlies ēst?");
        System.out.println("Ievadi 'q', lai izietu");
    }
}
