import java.time.LocalDateTime;

public class Salad extends Meal {
    public Salad() {
        super(16, 20, MealType.DINNER);
    }

    @Override
    public String getName() {
        return "Salad";
    }

    @Override
    public String getInstrument() {
        return "Fork";
    }
}
