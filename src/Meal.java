import java.time.LocalDateTime;

public abstract class Meal implements Eatable {
    private int from;
    private int to;
    private MealType type;

    public Meal(int from, int to, MealType type) {
        this.from = from;
        this.to = to;
        this.type = type;
    }

    public abstract String getName();

//    Homework
//    public boolean isInStock() {
//        return MealStockService.isInStock(getName());
//    }

//    public void increaseStock() {
//        MealStockService.increaseStock(getName());
//    }


//    public void decreaseStock() {
//        MealStockService.decreaseStock(getName());
//    }

    @Override
    public boolean canBeEaten(LocalDateTime time) {
        int currentHour = time.getHour();
        if (currentHour >= from && currentHour < to) {
            return true;
        }
        return false;
    }

    @Override
    public void eat() {
        if (canBeEaten(LocalDateTime.now())) {
            System.out.println("Eating " + getName() +" with " + getInstrument() + "!");
//            decreaseStock()
        } else {
            throw new CantEatException();
        }
    }

    public MealType getType() {
        return type;
    }

    public void setType(MealType type) {
        this.type = type;
    }
}
