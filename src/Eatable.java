import java.time.LocalDateTime;

public interface Eatable {
    boolean canBeEaten(LocalDateTime time);
    void eat();
    String getInstrument();
    default void printInstrument() {
        System.out.println(getInstrument());
    }
}